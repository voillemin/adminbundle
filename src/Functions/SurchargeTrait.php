<?php


namespace emforbfc\AdminBundle\Functions;

Trait SurchargeTrait
{
    /*
     * Fonction de surcharge qui permet d'attribuer une valeur à une colonne dans datatable bien spécifique. (Doit être surchargée dans le controller voulu.)
     */
    protected function getColumnValue($key,  $entity, $options=[]){
        $return = null;

        if(isset($key) &&  isset($entity[$key])){
            $return = $entity[$key];
        }
        return $return;
    }

    /*
     * Fonction de surcharge qui permet de set un element bien spécifique dans editAction. (A surcharger dans le controller voulu).
     */
    protected function specificSetFunction($request, $objectManager, $entity,$id) {
        return null;
    }

    /*
  * Fonction de surcharge qui permet d'ajouter dans les logs la suppression par un utilisateur.
  */
    protected function specificSetFunctionDelete() {
        return null;
    }

    /*
 * Fonction de surcharge qui permet d'ajouter une action dans le tableau.
 */
    protected function specificSetEdit($entity) {
        $val = null;
return $val;
    }

    /*
     * Fonction de surcharge des paramètres à envoyer au template Edit twig.
     */
    protected function getEditParam($entity) : array
    {
        return [];
    }

    /*
     * Fonction de surcharge des paramètres à envoyer au template New twig.
     */
    protected function getNewParam($instanceClass) : array {
        return [];
    }

}