<?php


namespace emforbfc\AdminBundle\Functions;


use emforbfc\AdminBundle\Resources\Classes\ConfFiltre;
use emforbfc\AdminBundle\Resources\Classes\ConfOnglet;

Trait ConfigTrait
{
    private $configuration;

    public function __construct()
    {
        $this->configuration = ['colonnes' => ['id' => array('title' => 'Id', 'type' => "int", 'visible' => 'non'),
            'action' => array('title' => 'Actions', 'type' => "modifier", 'visible' => 'oui')
        ], 'filtres' => new ConfFiltre(), 'onglets' => new ConfOnglet()];
    }

    final private function getConf()
    {
        return $this->configuration;
    }

    protected function setConfColonnes($arrayObj){
        $this->configuration['colonnes'] = $arrayObj;
    }

    protected function setConfFiltres($arrayObj){
        $this->configuration['filtres'] = $arrayObj;
    }

    protected function setConfOnglets($arrayObj) {
        $this->configuration['onglets'] = $arrayObj;
    }

    protected function getConfColonne(){
        return $this->configuration['colonnes'];
    }

    protected function getConfFiltres(){
        return $this->configuration['filtres'];
    }

    protected function getConfOnglets() {
        return $this->configuration['onglets'];
    }
}