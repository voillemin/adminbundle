function myDatatable(page, idTable, dataForm, myColumnsTwig) {
    return $(idTable).DataTable({
        serverSide: true,
        processing: true,
        ajax:{
            type: "POST",
            url: "/"+page+"/list",
            data:  function(d) {
                d.filtre = dataForm;
            }
        },
        headers: {"Authorization": localStorage.getItem('token')},
        columns: myColumnsTwig,
        columnDefs: [
            {"className": "dt-center", "targets": "_all"}
        ],
        language: {
            url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
        }
    });
}