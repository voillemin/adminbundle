<?php

namespace App;

use emforbfc\AdminBundle\Controller\CrudTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
/**
 * Class CrudController
 * @package App\Controller
 * @Route("/admin/")
 */
class CrudController extends AbstractController
{
    use CrudTrait;

    const PAGE = null;
    const PAGE_LIBELLE = self::PAGE;
}
